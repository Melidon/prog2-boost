#include <gtest/gtest.h>

#include "Complex.h"

TEST(Complex, AbsoluteValue) {
    Complex a(3, 4);
    EXPECT_EQ(a.abs(), 5);
}

TEST(Complex, Argument) {
    Complex a(1, 1);
    EXPECT_NEAR(a.arg(), 0.785, 0.01);
}

TEST(Complex, Conjugate) {
    Complex a(1, 2);
    Complex b = a.conj();
    EXPECT_EQ(b.real(), 1);
    EXPECT_EQ(b.imag(), -2);
}

TEST(Complex, Addition) {
    Complex a(1, 2);
    Complex b(3, 4);
    Complex c = a + b;
    EXPECT_EQ(c.real(), 4);
    EXPECT_EQ(c.imag(), 6);
}

TEST(Complex, Subtraction) {
    Complex a(1, 2);
    Complex b(3, 4);
    Complex c = a - b;
    EXPECT_EQ(c.real(), -2);
    EXPECT_EQ(c.imag(), -2);
}

TEST(Complex, Multiplication) {
    Complex a(1, 2);
    Complex b(3, 4);
    Complex c = a * b;
    EXPECT_EQ(c.real(), -5);
    EXPECT_EQ(c.imag(), 10);
}

TEST(Complex, Division) {
    Complex a(1, 2);
    Complex b(3, 4);
    Complex c = a / b;
    EXPECT_NEAR(c.real(), 0.44, 0.01);
    EXPECT_NEAR(c.imag(), 0.08, 0.01);
}

TEST(Complex, Equality) {
    Complex a(1, 2);
    Complex b(1, 2);
    Complex c(2, 3);
    EXPECT_EQ(a, b);
    EXPECT_NE(a, c);
}
