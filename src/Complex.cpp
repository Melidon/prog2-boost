#include <cmath>

#include "Complex.h"

Complex::Complex(double real, double imag) : real_(real), imag_(imag) {}

double Complex::real() const { return real_; }

double Complex::imag() const { return imag_; }

double Complex::abs() const { return std::sqrt(real_ * real_ + imag_ * imag_); }

double Complex::arg() const { return std::atan2(imag_, real_); }

Complex Complex::conj() const { return Complex(real_, -imag_); }

Complex operator+(const Complex &lhs, const Complex &rhs) {
    return Complex(lhs.real() + rhs.real(), lhs.imag() + rhs.imag());
}

Complex operator-(const Complex &lhs, const Complex &rhs) {
    return Complex(lhs.real() - rhs.real(), lhs.imag() - rhs.imag());
}

Complex operator*(const Complex &lhs, const Complex &rhs) {
    return Complex(lhs.real() * rhs.real() - lhs.imag() * rhs.imag(),
                   lhs.real() * rhs.imag() + lhs.imag() * rhs.real());
}

Complex operator/(const Complex &lhs, const Complex &rhs) {
    double denom = rhs.real() * rhs.real() + rhs.imag() * rhs.imag();
    return Complex((lhs.real() * rhs.real() + lhs.imag() * rhs.imag()) / denom,
                   (lhs.imag() * rhs.real() - lhs.real() * rhs.imag()) / denom);
}

bool operator==(const Complex &lhs, const Complex &rhs) {
    return lhs.real() == rhs.real() && lhs.imag() == rhs.imag();
}

bool operator!=(const Complex &lhs, const Complex &rhs) {
    return !(lhs == rhs);
}
