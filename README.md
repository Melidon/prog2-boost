# Nagyházi kiinduló projekt

## Mi ez és miért?

A C++-ban való fejlesztés elsőre elég nehézkes tud lenni.
A Code::Blocks egy működő megoldás, de azért bőven akadnak vele nehézségek.
Ez a projekt egy szerintem jobban használható alternatívát kíván nyújtani.

Több okból is ajánlom:
- A Visual Studio Code-nak nagyon jó a debugger támogatása és a tesztek is könnyen futtathatóak.
- Fordításához egy CMake nevű program van használva. Csak bele kell írni, hogy melyik fájlokat szeretné fordítani az ember és már működik is.
- Nem a Szebi féle GTest-Light és Memtrace van használva, hanem a rendes GTest és AddressSanitizer.

## Hogyan töltsem le?

### Ha profi vagy

Akkor csak klónozd le a repót: `git clone git@gitlab.com:Melidon/prog2-boost.git`.
### Ha még nem használtál git-et

Akkor itt az ideje, hogy elkezdjél! De egyelőre az is jó lesz, ha csak [ZIP](https://gitlab.com/Melidon/prog2-boost/-/archive/master/prog2-boost-master.zip)-ként töltöd le a projektet.

## Mire van szükségem ahhoz, hogy működjön?

- Mint minden C++ projekt fordításához, ehhez is kell egy fordító például GCC vagy Clang.
- Szükséged lesz még CMake-re is. Windows esetében például [innen](https://cmake.org/install/) tudod letölteni.
- Szerezz egy Visual Studio Code-ot is. Windows esetében ezt [innen](https://code.visualstudio.com/Download) teheted meg.

## Hogyan fogom tudni működésre bírni?

A Visual Studio Code első induláskor megkérdezi, hogy szeretnéd-e feltelepíteni a projekthez ajánlott kiegészítéseket.
Erre mondj igent, sokat fognak segíteni a fejlesztésben.

Igazából készen is vagy, elvileg működnie kell mindennek.

Csak arra figyelj, hogy futtatáshoz nem az F5 gombot kell használni, hanem az alső menüsoron kell rámenni a kis bogár ikonra (_Launch the debugger for the selected target_) kell rámenni.
Erre alternatíva, hogy a fenti menüsoron a **Help** fülön belül rámész a **Show All Commands** elemre és ott kiválasztod a **CMake: Debug** opciót.
Ez amúgy a **Ctrl + F5** billentyűkombinációval is elérhető.

Ha valaki mégis nagyon az **F5**-öt szeretné használni, akkor arról [itt](https://github.com/microsoft/vscode-cmake-tools/blob/main/docs/debug-launch.md#debug-using-a-launchjson-file) olvashat, hogy miként lehet beállítani.

## Hogyan tudok új forrásfájlokat hozzáadni?

A header fájlokat pakoljad be az include mappába. A cpp forrásfájlok pedig kerüljenek az src mappába. Pont úgy kell tenned, ahogy a Complex.h és Complex.cpp-nél van a példában.

Még egészítsed ki a CMakeLists.txt-ben a forrásfájlos részt. Csak pakold be az új forrásfájlokat a Complex.cpp alá ahhoz hasonlóan. Fontos, hogy a headerek ide nem kellenek!
