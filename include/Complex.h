#pragma once

/**
 * @brief Represents a complex number with real and imaginary parts.
 */
class Complex {
public:
    /**
     * @brief Constructs a Complex number object.
     *
     * @param real The real part of the Complex number (default: 0).
     * @param imag The imaginary part of the Complex number (default: 0).
     */
    Complex(double real = 0, double imag = 0);

    /**
     * @brief Retrieves the real part of the Complex number.
     *
     * @return The real part of the Complex number.
     */
    double real() const;

    /**
     * @brief Retrieves the imaginary part of the Complex number.
     *
     * @return The imaginary part of the Complex number.
     */
    double imag() const;

    /**
     * @brief Calculates the absolute value (magnitude) of the Complex number.
     *
     * @return The absolute value (magnitude) of the Complex number.
     */
    double abs() const;

    /**
     * @brief Calculates the argument (phase) of the Complex number.
     *
     * @return The argument (phase) of the Complex number.
     */
    double arg() const;

    /**
     * @brief Calculates the complex conjugate of the Complex number.
     *
     * @return The complex conjugate of the Complex number.
     */
    Complex conj() const;

private:
    /**
     * @brief The real part of the Complex number.
     */
    double real_;

    /**
     * @brief The imaginary part of the Complex number.
     */
    double imag_;
};

/**
 * @brief Adds two Complex numbers.
 *
 * @param lhs The left-hand side Complex number.
 * @param rhs The right-hand side Complex number.
 * @return The result of the addition as a new Complex number.
 */
Complex operator+(const Complex &lhs, const Complex &rhs);

/**
 * @brief Subtracts two Complex numbers.
 *
 * @param lhs The left-hand side Complex number.
 * @param rhs The right-hand side Complex number.
 * @return The result of the subtraction as a new Complex number.
 */
Complex operator-(const Complex &lhs, const Complex &rhs);

/**
 * @brief Multiplies two Complex numbers.
 *
 * @param lhs The left-hand side Complex number.
 * @param rhs The right-hand side Complex number.
 * @return The result of the multiplication as a new Complex number.
 */
Complex operator*(const Complex &lhs, const Complex &rhs);

/**
 * @brief Divides two Complex numbers.
 *
 * @param lhs The left-hand side Complex number (dividend).
 * @param rhs The right-hand side Complex number (divisor).
 * @return The result of the division as a new Complex number.
 */
Complex operator/(const Complex &lhs, const Complex &rhs);

/**
 * @brief Checks if two Complex numbers are equal.
 *
 * @param lhs The left-hand side Complex number.
 * @param rhs The right-hand side Complex number.
 * @return True if the Complex numbers are equal, False otherwise.
 */
bool operator==(const Complex &lhs, const Complex &rhs);

/**
 * @brief Checks if two Complex numbers are not equal.
 *
 * @param lhs The left-hand side Complex number.
 * @param rhs The right-hand side Complex number.
 * @return True if the Complex numbers are not equal, False otherwise.
 */
bool operator!=(const Complex &lhs, const Complex &rhs);
